       IDENTIFICATION DIVISION.
       PROGRAM-ID. Triangle2.
       AUTHOR. eiei.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  LINE-SC           PIC X(80) VALUE SPACES.
       01  STAR-NUM          PIC 9(3)  VALUE ZERO.
           88 STAR-VALID     VALUE 1  THRU 80.
       01  INDEX-NUM1        PIC 9(3) VALUE  ZERO.
       01  INDEX-NUM2         PIC 9(3) VALUE  ZERO.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 001-INPUT-STAR-NUM THRU  001-EXIT 
           PERFORM  VARYING INDEX-NUM1 FROM 1 BY 1
              UNTIL INDEX-NUM1 > STAR-NUM
              COMPUTE INDEX-NUM2  = STAR-NUM  - INDEX-NUM1 + 1
              MOVE ALL "*" TO LINE-SC (INDEX-NUM2 : INDEX-NUM1 )
              DISPLAY  LINE-SC 
              END-PERFORM
           GOBACK 
           .


       

       001-INPUT-STAR-NUM.
           PERFORM UNTIL STAR-VALID
              DISPLAY "Please input star number: " WITH NO ADVANCING
              ACCEPT STAR-NUM
              IF NOT STAR-VALID  DISPLAY "Please input star number in p
      -       "ositive number"
           END-PERFORM
           .        

       001-EXIT.
           EXIT.