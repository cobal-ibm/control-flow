       IDENTIFICATION DIVISION.
       PROGRAM-ID. Triangle3.
       AUTHOR. eiei.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  LINE-SC           PIC X(80) VALUE SPACES.
       01  STAR-NUM          PIC 9(3)  VALUE ZERO.
           88 STAR-VALID     VALUE 1  THRU 80.
       01  INDEX-NUM1        PIC 9(3) VALUE  ZERO.
       01  INDEX-NUM2         PIC 9(3) VALUE  ZERO.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 002-INPUT-STAR-NUM THRU 002-EXIT 
           PERFORM 001-PRINT-STAR-LINE THRU 001-EXIT VARYING INDEX-NUM1 
              FROM STAR-NUM  BY -1 UNTIL INDEX-NUM1 = 0
           .

       001-PRINT-STAR-LINE.
           MOVE ALL SPACES TO LINE-SC 
           MOVE ALL "*" TO LINE-SC(1:INDEX-NUM1) 
           DISPLAY LINE-SC 
           .        

       001-EXIT.
           EXIT.


       002-INPUT-STAR-NUM.
           PERFORM UNTIL STAR-VALID
              DISPLAY "Please input star number: " WITH NO ADVANCING
              ACCEPT STAR-NUM
              IF NOT STAR-VALID  DISPLAY "Please input star number in po
      -       "sitive number"
           END-PERFORM
           .        

       002-EXIT.
           EXIT.          