       IDENTIFICATION DIVISION.
       PROGRAM-ID. Triangle1.
       AUTHOR. eiei.

       DATA DIVISION.
       WORKING-STORAGE SECTION.

       01  LINE-SC           PIC X(80) VALUE SPACES.
       01  STAR-NUM          PIC 9(3)  VALUE ZERO.
           88 STAR-VALID     VALUE 1  THRU 80.
       01  INDEX-NUM         PIC 9(3) VALUE  ZERO.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM 002-INPUT-STAR-NUM THRU  002-EXIT 
           PERFORM 001-PRINT-STAR-LINE THRU  001-EXIT 
              VARYING INDEX-NUM FROM 1 BY 1
              UNTIL INDEX-NUM > STAR-NUM
              
              
           GOBACK 
           .


       001-PRINT-STAR-LINE.
           MOVE ALL "*" TO LINE-SC(1:INDEX-NUM ) 
           DISPLAY LINE-SC 
           .

       001-EXIT.
           EXIT.

       002-INPUT-STAR-NUM.
           PERFORM UNTIL STAR-VALID
              DISPLAY "Please input star number: " WITH NO ADVANCING
              ACCEPT STAR-NUM
              IF NOT STAR-VALID  DISPLAY "Please input star number in p
      -       "ositive number"
           END-PERFORM
           .        

       002-EXIT.
           EXIT.